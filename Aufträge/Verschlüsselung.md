### Verschlüsselung

---

**Was ist eine Verschlüsselung**

Bei der Verschlüsselung werden die Daten/ Nachricht **nicht lesbar**. Um diese zu entschlüsseln muss man den dafür gebrauchten key haben, um die Daten/ Nachricht zu entschlüsseln und es somit lesbar machen. Dies ist der Vorgang der Verschlüsselung.



---

### Ablauf

- Kleopatra Herunterladen

- Login

- Text erstellen und verschlüsseln

- Passwort einrichten

- Empfänger hinzufügen

- Die Nachricht and Empfänger schicken (mit dem Passwort)

- Dann kann der Empfänger im Kleopatra die verschlüsselte Nachricht entschlüsseln.


---
**Fazit**


Mit der Verschlüsselung kann man Nachrichten sicher austauschen, ohne das jemand drittes mithört. Kleopatra ist ein Tool, dass das verschlüsseln und entschlüsseln vereinfacht.