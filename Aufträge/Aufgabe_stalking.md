# Informationsliste

## Einleitung
- In Hamburg an einem Dienstagmorgen wurde eine 22. Jährige Frau ermordet.

## Opfer
- Antonia. H
- 22 Jahre alt
- Studentin
- Stalkingopfer

![Opfer](/img/Opfer2.PNG )

## Täter
- Kim. W
- Jungpolitiker
- 22 Jahre alt
- verliebt
- Mörder

![Täter](/img/Taeter2.png)


## Tat

- Kim. W stalkte Antonia. H 9 Monate lang. Er sprach sie öfters an, doch ohne Erfolg. Eines Tages mietete er ein Hotel in Ihrer nähe und tötete sie in Ihrem Treppen Haus und anschliessend sich selbst.

