# Backup/Restore

**Was ist überhaupt ein “Backup”?**
Als Backup versteht man eine Sicherung von einer Detei. Somit kann man sich einen Datenverlust erlauben, da man eine Kopie an einem anderen Ort gespeichert hat.
**Welche Gründe gibt es, Backups zu erstellen?**
Wie schon erwähnt kann ein Backup eine Datei sichern. Falls mal Daten verloren gehen, kann man sie einfach wieder herstellen. Mit dem Backup hat man auch automatisch mehrere Versionen und kann so bei Fehler auf die alte Version zurückgreifen. Man kann sie auch in einer Cloud sichern, somit kann man von überall darauf zugreifen.
**Worauf kann gesichert werden und welche Vor-/Nachteile gibt es dabei?**
Eine Möglichkeit ist es eine Datei in der Cloud zu sichern, somit kann man von überall darauf zugreifen. Man kann sie auch lokal speichern, somit kann man nicht von überall zu greifen. Man braucht aber kein Internet und darauf zugreifen zu können.
**Welche Backup-Verfahren gibt es?**
- Vollständiger Backup

- inkrementelles Backup 

 - differenzielll

**Was wird unter dem “Generationsprinzip” verstanden?**

Der Sohn 4mal die Woche ein Backup durch.

Der Vater bildet jede Woche nur ein Backup durch.

Der Grossvater für jeden Monat ein Backup durch.

**Wie halten Sie es mit Backups? Machen Sie welche? Wie? Wann?**
Ich persönlich mache keine Backups, da ich auch keine besonders wichtigen Daten habe die gut gesichert werden sollen. Früher oder später muss dies geändert werde. In der Berufswelt ist das Backup von grosser Bedeutung.