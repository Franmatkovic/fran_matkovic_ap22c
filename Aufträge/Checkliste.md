# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

## Checkliste Pc Sicherheit erhöhen:
Link: https://www.datenschutz.ch/meine-daten-schuetzen/pc-sicherheit-erhoehen-in-fuenf-schritten

Persönliche Informationen schützen:
1. Geben Sie nicht mehr persönliche Informationen preis als unbedingt nötig.
- Ich versuche so wenig Informationen wie möglich über mich im Internet preiszugeben. Da ich nur auf ausgewählten Webseiten meine persönliche Daten angebe.
2. Surfen Sie anonym im Internet.
- Ich surfe nur sehr selten Anonym im Internet da ich es oftmals vergesse. Jedoch surfe ich immer mit einem VPN.
3. Löschen Sie temporäre Internetdateien (im Cache) und den Browserverlauf regelmässig.
- ich lösche mindestens einmal pro Woche meinen Suchverlauf und verhindere so das Tracking.

Angriffe abwehren:
1. Führen Sie Sicherheitsupdates regelmässig durch (insbesondere von Windows, MacOS, Java, Adobe Flash und Adobe Reader).
- Ich mache regelmässige Sicherheitsupdates auf meinem Laptop vorallem Windows updates die mir angezeigt werden. 
2. Aktivieren Sie Firewalls – auf Windows – auf MacOS).
- Firewalls auf Windows deaktiviere ich sowieso nie und Programme die das fordern installiere ich auch nicht.
3. Wählen Sie sichere Browsereinstellungen.
- Ich denke ich haben die sichersten Browsereinstellungen und surfe auch nicht auf unsicheren Webseiten.
4. Installieren Sie Virenschutzprogramme, zum Beispiel Microsoft SE, Avira, oder Avast
- Ich habe keine externen Virenschutzprogramme da ich finde, wenn man nicht auf unsicheren Seiten surft, reicht der Windows Defender.

Zugriffe Unberechtigter verhindern:
1. Verwenden Sie sichere Passwörter. Dabei hilft Passwortcheck.
- Ich verwende überall nur generierte Passwörter die sicher sind und nirgends das gleiche.
2. Nutzen Sie fremde drahtlose Netzwerke mit Vorsicht.
- Ich verwende so gut wie nie fremde Netzwerke meistens wenn ich unterwegs bin nutze ich mein Hotspot und sonst aktiviere ich auch meinen VPN für mehr Sicherheit.
3. Verschlüsseln Sie Ihr eigenes drahtloses Netzwerk.
- Ausser mit einem VPN veschlüssel ich mein Netzwerk nicht.
4. Nutzen Sie die Bildschirmsperre. 
- Ich sperre mein Bildschirm immer wenn ich mein Arbeitsplatz verlasse damit keiner meine Daten missbrauchen kann.

Verschlüsseln Sie sensitive Inhalte:
1. Verschlüsseln Sie Datenträger. Anleitung für Windows und MacOS
- Mein externer Datenträger verschlüssle ich mit BitLocker.
2. Verschlüsseln Sie E-Mails mit sensitivem Inhalt.
- Ich verschlüssle meine E-Mails eher selten habe aber eine private E-Mail für private und sensible Inhalte.

Sichern Sie Informationen und löschen Sie Daten vollständig:
1. Führen Sie regelmässig Datensicherungen (Back-ups) durch.
- Ich lade jegliche Dateien auf mein OneDrive hoch und so werden sie immer direkt abgespeichert und gesichert.
2. Bewahren Sie Sicherheitskopien feuer- und diebstahlsicher auf.
- Meinen externen Datenträger habe ich immer bei mir Zuhause wenn ich ihn nicht brauche.
1. Löschen Sie Informationen vor Entsorgung oder Verkauf des PCs vollständig, zum Beispiel unter Verwendung spezieller Software. 
- Bis jetzt habe ich immer all meine Daten sicher heruntergeladen und übertragen und danach den PC zurückgesetzt bevor ich ihn verkauft habe.
  
## Checkliste SmartPhone Sicherheit erhöhen:
Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen

Gerätesperre aktivieren:
1. Aktivieren Sie die zeitabhängige Gerätesperre (zum Beispiel nach zwei Minuten). Nutzen Sie PIN, Passwort, Fingerabdruckleser oder Gesichtserkennung zum Entsperren. Geben Sie PINs oder Passwörter nur unter Sichtschutz ein.
- Ich habe auf meinem Handy die Face ID aktiviert und falls diese nicht funktioniert habe ich noch einen PinCode.

Gerät bei Verlust sofort sperren und löschen lassen:
1. Nutzen Sie die Remote-Wipe-Funktion, mit der das Gerät aus der Ferne zurückgesetzt werden kann. Lassen Sie danach die SIM-Karte Ihres Geräts unverzüglich sperren.
- Ich habe noch nie mein Handy verloren jedoch wenn es so währe würde ich sofort meinem Handy-Andbieter anrufen und die SIM Karte sperren lassen.

Apps aus vertrauenswürdigen Quellen installieren:
1. Falls Ihnen der Anbieter einer App nicht bekannt ist, führen Sie vor der Installation eine Suche im Internet durch. Diese deckt nicht vertrauenswürdige Anbieter bzw. Applikationen mit Schadsoftware in der Regel schnell auf. Je nach Hersteller erfahren Sie bei der App-Installation, welche Zugriffsrechte eine App anfordert. Überlegen Sie, ob die Zugriffsrechte zum Erfüllen der Funktionalität wirklich notwendig sind.
- Ich lade ausschlieslich Apps aus dem App-Store herunter da ich alles andere unsicher finde.
  
Öffentliche Funknetzwerke mit Vorsicht nutzen:
1. Nutzen Sie öffentliche Hotspots mit erhöhter Vorsicht. Führen Sie auf öffentlichen Hotspots keine kritischen Anwendungen wie Online-Banking durch.
- Öffentliche Hotspots verwende ich sogut wie nie. Ich habe ein gutes Handy Abo und brauche es daher nicht. Ausserdem finde sind öffentliche Netzwerke meistens unsicher und anfällig.

Updates regelmässig durchführen:
1. Kontrollieren Sie, ob es Sicherheitsupdates für Ihr Gerät (Firmware, Betriebssystem) oder für von Ihnen installierte Apps gibt, und führen Sie diese durch. Neuere Geräte erledigen die Suche nach Updates automatisch.
- Bei meinem Handy Update ich immer regelmässig wenn mir mein Handy eine Meldung gibt. IOS Update sind wichtig damit die Sicherheit weiterhin gewährleistet werden kann.

Daten vor Verkauf oder Entsorgung komplett löschen:
1. Verhindern Sie, dass Ihre gespeicherten Daten beim Verkauf oder bei der Entsorgung Ihres Gerätes in falsche Hände geraten. Löschen Sie vorher alle Datenspeicher.
- Auch hier gilt: Alle Dateien vor dem Verkauf sichern und danach über das Betriebseigene System das Handy zurücksetzten. 

Drahtlose Schnittstellen deaktivieren:
1. Deaktivieren Sie drahtlose Schnittstellen (z.B. WLAN, Bluetooth und NFC), wenn Sie diese nicht nutzen. Dies hilft auch, den Stromverbrauch zu senken. Denken Sie dabei auch an die Ortungsdienste.
- Das habe ich bis jetzt noch gar nicht gemacht und sollte ich mir angewöhnen. Da Ich bis jetzt nicht darüber bescheid wusste.