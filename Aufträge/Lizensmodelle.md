## Lizenzmodelle

Software zu erstellen ist aufwändig und teuer. Damit diese Arbeit sich auch lohnt, gibt es verschiedene Möglichkeiten zu verhindern, dass diese einfach so weiterverbreitet werden. Die Art der Anbietung wird über eine **Lizenz** geregelt.

### Was ist eine Lizenz?

Eine Lizenz kann ein Progromm oder sonst was schützen. Somit kann man in den meisten Fällen ein Programm ohne die benötigte Lizenz nicht nutzen. Für so eine Lizenz muss man im Öfteren auch zahlen.

### Welche Arten von Software Lizenzen gibt es?

**1. Public Domain**

Eine Public Domain ist ein urheberrechtsfreie Lizenzart für Software. Sie ist gemeinfrei, was so viel bedeutet, wie jeder darf sie uneingeschränkt und kostenlos verwenden.

**2. Open Source Lizenz**

Open Spurce heisst, dass der Code des Programmes sichtbar/ zugänglich ist. Dies heisst aber nicht, dass die Software konstenlos sein muss.

**3. General Public Lizenz (GNU)**

General Public Lizenz ist eine Softwarelizenz, die einem die Möglichkeit gibt, die Software auszuführen, zu ändern und zu verbreiten. Man kann die Software also frei verwenden.

**4. Dauerlizenz**

Mit der Dauerlizenz kann man die Software unbegrenzt nutzen.
**5. Arbeitsstation Lizenz**

Die Software darf nur auf einem dafür bestimmten Computer verwendet werden. Die Nutzung ist also an den physischen Computer gebunden. Der Erwerber dieser Lizenz ist jedoch berechtigt, eine Sicherungskopie der Software anzulegen .

**6. Proprietäre Lizenz**

Die Bedeutung dieser Lizenz heisst, dass man eine oder mehrere Kopien der Anwendung machen darf. Die Software bleibt aber dennoch das Eigentum des Herstellers.

**7. Kommerzielle Software**

Hier werden die Nutzungsrechte erworben. Beim Erwerb muss kein tatsächlicher Geldfluss vonstattengehen.

**8. Eula**

Ein EULA ist eine Vereinbarung, die einer Person oder einem Unternehmen das Recht einräumt, die Software auf eine bestimmte Weise zu nutzen.

**Fazit**

Es gibt viele Arten von Lizenzen. Lizenzen schützen Werke, durch sie kann nicht jeder einfach die Software weiterverbreiten oder weiterverkaufen. Die Art der Anbietung wird über eine Lizenz geregelt.
