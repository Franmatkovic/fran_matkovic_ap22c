# Passwortmanager


## Ein wichtiger bestandteil des Informatiker und online Surfer ist ein sicheres Passwort. Ein Passwortmanager ist ein gutes Hilfsmittel die Passwörter sicher zu speichern. ##

---
### *Schauen Sie nach, welche Passwort-Manager am besten bewertet werden:* ###

![am besten bewertete Passwortmanager](https://www.netzwoche.ch/sites/default/files/248039_2.png)

Das ist eine [Rangliste](https://www.experte.de/passwort-manager) von den besten Passwortmanager Programmen.


### Unter diesem Link finden sie die Bewertungen einzelner Passwort- Manager. Dazu kann man die Kommentare einzelner Kunden lesen. ###