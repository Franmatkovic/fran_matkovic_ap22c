## Authentifizierung

Mit der Authentifizierung identifiziert man sich. Mit einem Passwort, Fingerabdruck oder mit der Face ID kann man so zu sagen seine Identität nachweisen, um zum Beispiel den zugang zu einem Konto zu erhalten.



## Autorisierung

Die Autorisierung ist ein Erlaubnis. Man gibt so zu sagen jemanden Recht auf etwas. Ein gutes Beispiel dafür ist, wenn man sich ein GA Abo holt und man dann die Erlaubnis hat, überall in der Schweiz den Öv benutzen zu dürfen.