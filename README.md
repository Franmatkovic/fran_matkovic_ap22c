# Fran Matkovic



## **231 Datenschutz und Datensicherheit anwenden**

## Checklisten Auftrag

- Das ist der [Checklisten Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Checkliste.md)
Heute gaben wir unsere Grundkentnisse aus, in Form von einer Checkliste. Wo wir schriftlich festhieltne mit was wir in Vergangenheit bereits zu tun hatten und was wir bereits gelernt haben.


- Das ist der [Pinguinen Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Aufgabe_Pinguine.md)
Hier wurden unser Wissen über das Markdown geprüft. Für viele (mich mit einbezogen) war das ein grosses Fragezeichen, was sich jedoch nach diesem Auftrag änderte. In diesem Auftrag lernten wir das Grundwissen über Markdown. Dazu zählt auch das Formattiern, was erstunlich einfach war.

- Das ist der [stalking Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Aufgabe_stalking.md)
Wir erhielten ein Bericht über ein Stalking Opfer. In diesem Auftrag lasen wir den Bericht und recherchierten über denn Fall. Im Bericht waren nur mangelnde Informationen, die aber mit gutem Recherchieren sich immer wuch bis es schliesslich ein grosser Haufen an Informationen wurde. Dies mussten wir in einer Markdown Datei festhallten und lernten wie beängstigend leicht man an Informationen kommen kann. 


- Das ist der [Cookies Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Webseite_verwendet_Cookies.pdf)
Im Auftrag Webseite verwendet Cookies habe ich in Partnerarbeit mit dem Herr Martin über Cookies recherchiert und geschreiben. Ich habe gelernt, wofür Cookies gebraucht werden und wie sie funktionieren.



- Das ist der [Authentifizierung/ Autorisierung Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Webseite_verwendet_Cookies.pdf) Auftrag
Heute haben wir uns mit der Authentifizierung und der Autorisierung genauer ausseinander gesetzt und gelernt, was das bedeutet und was ihr Nutze ist.




- Das ist die [Auswertung ](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Foxtrail_M231_01.pdf) vom Quiz.

Hier konnten wir in zweier Gruppen unser Wissen über die Verschlüsselung und vieles mehr in Beweis stellen. Das Ziel war es alle Aufgaben in kürzester Zeit zu lösen.


- Das ist der [Hacker Auftrag. ](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Hacken.md)
Heute haben wir uns mit der Authentifizierung genauer ausseinander gesetzt und gelernt, was das bedeutet und was ihr Nutze ist.


- Das ist der [Lizenz Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Lizensmodelle.md)
Hier haben wir unser Wissen über die Lizenzen erweitert und gelernt was ihr Nutzen ist und auf was wir achten muss.



- Das ist der [Verschlüsselungs Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/Verschl%C3%BCsselung.md) 
Wie der Name schon sagt handelt es sich in diesem Auftrag um die Verschlüsselung und Entschlüsselung von Daten/ Nachrichten. Dazu lernten wir was für Schlüssel/Key man für die Entschlüsselung braucht.


- Das ist der [Backup Auftrag.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/backup.md)
Hier lernten wir was ein Backup ist, wofür man es braucht und warum man sich ein Backup einrichten sollte.


- Das ist die [Excel tabelle.](https://gitlab.com/Franmatkovic/fran_matkovic_ap22c/-/blob/main/Auftr%C3%A4ge/backup.md)
In  diesem Auftrag haben wir persöhnliche Geräte genauer angeschaut und ihre technischen Daten in einer Excel Tabelle festgehalten.